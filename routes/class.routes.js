const express = require('express');
const router = express.Router();
const classCtrl = require('../controllers/class.ctrl');

router.get('/', classCtrl.all);
router.get('/:id', classCtrl.detail);
router.post('/', classCtrl.post);
router.put('/:id', classCtrl.edit);
router.delete('/:id', classCtrl.delete);

module.exports = router;
