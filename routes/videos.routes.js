const express = require('express');
const router = express.Router();
const videosCtrl = require('../controllers/videos.ctrl');

router.get('/', videosCtrl.all);
router.get('/:id', videosCtrl.detail);
router.post('/', videosCtrl.post);
router.put('/:id', videosCtrl.edit);
router.delete('/:id', videosCtrl.delete);

module.exports = router;
