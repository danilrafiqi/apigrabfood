const Class = require('../db/models/class');
module.exports = {
  all: (req, res) => {
    Class.find({}, (err, data) => {
      if (err) throw err;
      res.send(data);
    });
  },
  detail: (req, res) => {
    Class.findById(req.params.id, (err, data) => {
      if (err) throw err;
      res.send(data);
    });
  },
  post: (req, res) => {
    Class.create(req.body, (err, data) => {
      if (err) throw err;
      res.send('success create data');
    });
  },
  edit: (req, res) => {
    Class.findByIdAndUpdate(req.params.id, req.body, (err, data) => {
      if (err) throw err;
      res.send('succes edit');
    });
  },
  delete: (req, res) => {
    Class.findByIdAndRemove(req.params.id, err => {
      if (err) throw err;
      res.send('success delete');
    });
  }
};
