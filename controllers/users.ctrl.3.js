const User = require('../db/models/users');
module.exports = {
  all: (req, res) => {
    User.find({}, (err, data) => {
      if (err) throw err;
      res.send(data);
    });
  },
  detail: (req, res) => {
    User.findById(req.params.id, (err, data) => {
      if (err) throw err;
      res.send(data);
    });
  },
  post: (req, res) => {
    User.create(req.body, (err, data) => {
      if (err) throw err;
      res.send('success create data');
    });
  },
  edit: (req, res) => {
    User.findByIdAndUpdate(req.params.id, req.body, (err, data) => {
      if (err) throw err;
      res.send('succes edit');
    });
  },
  delete: (req, res) => {
    User.findByIdAndRemove(req.params.id, err => {
      if (err) throw err;
      res.send('success delete');
    });
  }
};
