const Videos = require('../db/models/videos');
module.exports = {
  all: (req, res) => {
    Videos.find({})
      .populate('classId')
      .exec((err, data) => {
        if (err) throw err;
        res.send(data);
      });
  },
  detail: (req, res) => {
    Videos.findById(req.params.id)
      .populate('classId')
      .exec((err, data) => {
        if (err) throw err;
        res.send(data);
      });
  },
  post: (req, res) => {
    Videos.create(req.body, (err, data) => {
      if (err) throw err;
      res.send('success create data');
    });
  },
  edit: (req, res) => {
    Videos.findByIdAndUpdate(req.params.id, req.body, (err, data) => {
      if (err) throw err;
      res.send('succes edit');
    });
  },
  delete: (req, res) => {
    Videos.findByIdAndRemove(req.params.id, err => {
      if (err) throw err;
      res.send('success delete');
    });
  }
};
