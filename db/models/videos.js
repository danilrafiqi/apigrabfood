const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const videoSchema = new Schema(
  {
    name: { type: String, required: true },
    urlVideo: { type: String, required: true },
    slug: { type: String, required: true },
    classId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Class'
    }
  },
  {
    timestamps: true
  }
);

const Video = mongoose.model('Video', videoSchema);

module.exports = Video;
