const createError = require('http-errors');
const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const cors = require('cors');

mongoose.connect(
  'mongodb://danil:rahasia12@ds129374.mlab.com:29374/lampungdev',
  { useNewUrlParser: true }
);

const route = require('./routes/index');

const app = express();
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(route);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

/* LISTENING */
const port = process.env.PORT || 8000;
app.listen(port, process.env.IP, () => {
  console.log('Server Started on Port: ', port);
});
